FROM openjdk:8-alpine
WORKDIR /tmp
COPY target/user-auth-0.0.1-SNAPSHOT.jar /tmp/user-auth-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "-Xms128m", "-Xmx256m", "/tmp/user-auth-0.0.1-SNAPSHOT.jar"]
EXPOSE 8083
