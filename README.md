# user-auth-service

**Pre requisites:-**

- Java Developement IDE
- Maven
- Git

This service register the user and authenticate the user to return a JWT token which is required to pass in headers for every request. 

**Features :-**

- Caching
- Swagger UI
- Exception Handling
- API Security
- Logging
- Dockerfile
- CI/CD Proposal through Jenkinsfile

**Run Through Docker:-**

1. Building your Docker image

_docker build -t user-auth:0.0.1-SNAPSHOT user-auth/._

2. Running your microservices in Docker containers

_docker run -d --name user-auth -p 8083:8083 user-auth:0.0.1-SNAPSHOT_

**Configure CI/CD Pipeline through Jenkinsfile:-**

Select "Add a Pipeline script from SCM" in Pipeline Section to add the Jenkinsfile that will auto create the stage in the jenkins pipeline.





